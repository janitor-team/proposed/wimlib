wimlib (1.13.4-1) unstable; urgency=medium

  * New upstream version 1.13.4

 -- Hilko Bengen <bengen@debian.org>  Tue, 20 Apr 2021 09:21:23 +0200

wimlib (1.13.3-1) unstable; urgency=medium

  * New upstream version 1.13.3
  * Add Multi-Arch lines
  * Bump Debhelper compat level

 -- Hilko Bengen <bengen@debian.org>  Sun, 15 Nov 2020 12:59:34 +0100

wimlib (1.13.2-1) unstable; urgency=medium

  * New upstream version 1.13.2

 -- Hilko Bengen <bengen@debian.org>  Sun, 24 May 2020 21:45:01 +0200

wimlib (1.13.1-1) unstable; urgency=medium

  * New upstream version 1.13.1

 -- Hilko Bengen <bengen@debian.org>  Mon, 02 Sep 2019 12:15:00 +0200

wimlib (1.13.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one
  * d/control: Set Vcs-* to salsa.debian.org

  [ Hilko Bengen ]
  * New upstream version 1.13.0
  * Rebase patches
  * Drop unneeded dh --parallel parameter
  * Bump Debhelper compat level
  * Replace dh_install --fail-missing
  * Bump Standards-Version
  * Add not-installed (dh_missing bug?)
  * Update .symbols file

 -- Hilko Bengen <bengen@debian.org>  Wed, 26 Dec 2018 12:58:51 +0100

wimlib (1.12.0-1) unstable; urgency=medium

  * New upstream version 1.12.0
  * Drop syslinug location patch in favor of upstream's solution
  * Modernize package: DH compat level, Standards-Version, Vvs-* URLs

 -- Hilko Bengen <bengen@debian.org>  Sat, 02 Sep 2017 22:25:30 +0200

wimlib (1.11.0-3) unstable; urgency=medium

  * Remove x32 FTBFS patch (#814138), it is no longer needed.

 -- Hilko Bengen <bengen@debian.org>  Fri, 26 May 2017 20:02:14 +0200

wimlib (1.11.0-2) unstable; urgency=medium

  * Add patch to fix syslinux module directory (Closes: #863397)
  * Add ifdef fix to enable building on x32 (Closes: #814138)

 -- Hilko Bengen <bengen@debian.org>  Fri, 26 May 2017 19:34:25 +0200

wimlib (1.11.0-1) unstable; urgency=medium

  * New upstream version 1.11.0

 -- Hilko Bengen <bengen@debian.org>  Mon, 23 Jan 2017 20:43:59 +0100

wimlib (1.10.0-1) unstable; urgency=medium

  * New upstream version
  * Update patch

 -- Hilko Bengen <bengen@debian.org>  Sat, 27 Aug 2016 10:30:57 +0200

wimlib (1.9.1-1) unstable; urgency=medium

  * New upstream version
  * Update patches

 -- Hilko Bengen <bengen@debian.org>  Sun, 20 Mar 2016 13:58:06 +0100

wimlib (1.9.0-2) unstable; urgency=medium

  * Do not create jquery symlink (see /usr/share/doc/doxygen/README.jquery)

 -- Hilko Bengen <bengen@debian.org>  Tue, 09 Feb 2016 01:01:21 +0100

wimlib (1.9.0-1) unstable; urgency=medium

  * New upstream version
  * Add missing doxygen build-dependency (Closes: #814136)
  * Only build against ntfs-3g-dev on Linux
  * Disable FUSE-related tests as they do not run correctly under fakeroot
    (Closes: #814151, #814153)

 -- Hilko Bengen <bengen@debian.org>  Mon, 08 Feb 2016 22:06:15 +0100

wimlib (1.8.3-1) unstable; urgency=low

  * Initial release (Closes: #759480)

 -- Hilko Bengen <bengen@debian.org>  Thu, 07 Jan 2016 23:11:27 +0100
